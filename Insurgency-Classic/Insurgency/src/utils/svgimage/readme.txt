This library, SVGImage, is copyright (c) 2006 Jethro G. Beekman.

Files:
svgimage.dll
svgimage.lib
svgimage.cpp
svgimage.h
svgimage.vcproj

Permission is granted ONLY for use in Insurgency: Modern Infantry Combat.

Other available licenses still pending.

Compiled with:
* cairo 1.2.2 (Win32-mod)
* libsvg 0.1.4 (Win32-mod)
* libsvg-cairo 0.1.6 (Win32-mod)
* expat 2.0.0 (Win32)
* zlib 1.2.3 (Win32)
* libpng 1.2.8 (Win32)
* jpeg 6b (Win32)

Modified sources of cairo, libsvg and libsvg-cairo available at request.