; Include global strings
!include "Global-Strings.nsh"

; ---------------------------------------
; English

; Version information
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Insurgency Installer"

; ---------------------------------------