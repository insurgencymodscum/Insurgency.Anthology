; Include global strings
!include "Global-Strings.nsh"

; ---------------------------------------
; English

; ModernUI Strings

; Version information
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Insurgency Updater"

; ---------------------------------------
; Language Neutral (must be last)
;!define MUI_WELCOMEPAGE_TITLE "$(STRING_WelcomeTitle)"
;!define MUI_WELCOMEPAGE_TEXT "$(STRING_WelcomeText)"