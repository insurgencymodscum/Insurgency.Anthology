Hello.
This is an installer generator I wrote for the mod.
If you need an installer generated, please use it.

It has some requirements, though.

	- Python 2.5
	  Which may be found at: http://www.python.org/download/
	- PySVN for Python 2.5
	  Which may be found at: http://pysvn.tigris.org/project_downloads.html
