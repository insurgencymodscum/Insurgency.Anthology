//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//

#ifndef GAME_H
#define GAME_H

#include "globals.h"

//=========================================================
//=========================================================
extern ConVar displaysoundlist;
extern ConVar mapcyclefile;
extern ConVar servercfgfile;
extern ConVar lservercfgfile;

//=========================================================
//=========================================================
extern const ConVar *g_pDeveloper;

#endif // GAME_H
