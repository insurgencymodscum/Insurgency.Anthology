//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef TE_FIREBULLETS_H
#define TE_FIREBULLETS_H
#ifdef _WIN32
#pragma once
#endif

//=========================================================
//=========================================================
extern void TE_FireBullets( int iPlayerIndex, const Vector &vecOrigin, const Vector &vecDir, int iSeed );

#endif // TE_FIREBULLETS_H
