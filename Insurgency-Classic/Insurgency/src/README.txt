=====================================================
=====================================================
====			  INSURGENCY CLASSIC			 ====
====			  ------------------			 ====
====	An Open-Source Mod for the Source Engine ====
=====================================================
=====================================================

===================================
====		INTRODUCTION	   ====
===================================

After a recent IP dispute over the INSURGENCY: Modern Infantry Combat (http://www.insmod.org) project, a total conversion
modification for Valve's Source Engine, the original developers have decided that it's in the best interest of the mod's
legacy to be released as an open-source project.

Many developers (over 40) have worked on the project in various forms, however there is still much room for improvement.
No one deserves to profit off of the mod, nor get in the way of its progress. By making the mod open source we are
releasing and supporting one of the more advanced open source game APIs that exist, allowing any programmer or developer 
that signs up to make the improvements and additions that they wish. As far as open source projects go, Insurgency provides 
developers with a great opportunity because of the game's countless complex, unfinished features; and its potential to be 
upgraded to Orange Box and improved upon significantly. 

This is the first revision of the Insurgency Classic project. Included is the full C++ Source of the game. As time goes on,
original developers will upload map source (.VMF), model source (.SMD/.QC) and 3ds max (.MAX) files, to allow developers to get
even more in-depth with additions to the game. We leave releases in the hands of the community that emerges. Once something
is compiled and stable, we encourage developers to upload the version to the Files section of the project homepage.

Questions, comments, concerns, or just want to discuss the project? Go here: http://forums.insurgencymod.net

A wiki is planned as well.

===================================
====	COMPILING & TESTING	   ====
===================================

To compile the Source Code, you must have a copy of Visual Studio 2003. Try newer versions of Visual Studio at your own risk.
We've only had success with 2003.

Compiling the Source will create DLL files, which should be put in your Steam/SteamApps/SourceMods/Insurgency/bin folder.
If you use the Insurgency Classic DLLs with the content in the most recent version of INSURGENCY: Modern Infantry Combat,
the game should work with the INS Classic code instead of the latest code.

For now all INS Classic is is code. Content may be added to the project as well if people wish to take it in that direction.

===================================
====		CHANGELOG		   ====
===================================

Revision 0 (10/14/10)
- Uploaded first revision of INS Classic Source Code, dating back to 2007
-

===================================
====		  CREDITS		   ====
===================================

Jeremy Blum - Project Director
Andrew Spearin - Design Director
James Mansfield - Lead Programmer
Jethro Beekman - Programmer
William Ewing - Programmer
Louka Outrebon - Programmer
Matthias Schmidt - Lead Level Designer
Chris Kay - Level Designer
Joey Adey - Level Designer
Jon Hickenbottom - Level Designer
Mark Eaton - Lead Artist
Thiago Klafke - Level Designer
Jay Lindsay - Lead Sound Designer
Kurt Stassen - Lead Level Designer
Douglas Hamilton - Level Designer
James Cantrell - Weapon Artist
Alex Wright - Animator
Xon Lu - Weapon Artist
Matt Stock - Artist
Ron Chow - Sound Designer
Chase Stone - Concept Artist
Guillaume Goddeeris - Designer
Jurryt Visser - Designer
Oliver Hobbs - Level Designer
Shawn Snelling - Artist
Jens Veith - Designer
Simon Burford - Level Designer
Brian Fieser - Sound Designer
Lasse Olsen - Texture Artist
Lasse Pederson - Environment Artist
Gustav Lundwall - Texture Artist
Jeroen van Werkhoven - Level Designer
Zachariah Scott - Film Director
Tom Reyneri - Animator (Rest in Peace buddy)

If you are not listed here and feel as though you should be, please contact us through the forums.

Copyright © 2002-2012 INSURGENCY™