#ifndef SVGTEXTURE_H
#define SVGTEXTURE_H

void SVGLoadLibrary();
void GetTextureSize(const char* pszTexture, int &iWidth, int &iHeight);

#endif