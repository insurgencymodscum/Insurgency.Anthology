//========= Copyright � 1996-2005, James "Pongles" Mansfield, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef STATS_GLOBAL_H
#define STATS_GLOBAL_H
#ifdef _WIN32
#pragma once
#endif

#define MAX_STATS_PLAYERS 32

#endif // STATS_GLOBAL_H