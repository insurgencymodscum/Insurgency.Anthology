#region Headers
//
// Global-Strike Program Info
// by Jeremy Blum
//

using System;

#endregion

namespace GlobalStrike
{
    static class GS_Root
    {

        #region Main Method

        static void Main(string[] args)
        {
            using (GS_Game game = new GS_Game())
            {
                game.Run();
            }
        }

        #endregion

    }
}