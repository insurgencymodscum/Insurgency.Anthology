﻿#region Description
//
// Global Strike Assembly Info
// by Jeremy Blum
//
#endregion

#region Includes
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
#endregion

#region AssemblyInfo
[assembly: AssemblyTitle("Global-Strike")]
[assembly: AssemblyProduct("Global-Strike")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("OG Software")]
[assembly: AssemblyCopyright("Copyright © OG Software 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("f4c6e17e-4e58-427c-9534-dd6968c99889")]
[assembly: AssemblyVersion("0.0.0.5")]
#endregion