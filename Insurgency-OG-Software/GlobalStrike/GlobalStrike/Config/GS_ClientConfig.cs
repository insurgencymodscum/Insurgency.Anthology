﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GlobalStrike.Config
{
    public class GS_ClientConfig : GS_ConfigFile
    {
        public GS_ClientConfig()
        {
            FileName = "config.editme";

            AddDefaultSettings();

            InputStream = new FileStream(
                "C:/Documents and Settings/blum/Desktop/Global-Strike/GlobalStrike/GlobalStrike/Config/",
                FileMode.OpenOrCreate, FileAccess.Read);
            Input = new StreamReader(InputStream);
        }

        public void AddDefaultSettings()
        {
            AddSetting(new GS_ConfigSetting("Config File", "C:/Documents and Settings/blum/Desktop/Global-Strike/GlobalStrike/GlobalStrike/Config/" + FileName));
            AddSetting(new GS_ConfigSetting("Log File", "C:/Documents and Settings/blum/Desktop/Global-Strike/GlobalStrike/GlobalStrike/log.txt"));
        }
    }
}
