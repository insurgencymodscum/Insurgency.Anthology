using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
/*
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Input;
using GlobalStrike.Renderer;
*/

namespace GlobalStrike.Config
{
    public class GS_ConfigFileManager : GameComponent
    {
        public GS_ClientConfig ClientConfigFile;
        public GS_ServerConfig ServerConfigFile;
        List<GS_ConfigSetting> MasterSettingsList;

        public GS_ConfigFileManager(GS_Game game)
            : base(game)
        {
            ClientConfigFile = new GS_ClientConfig();
            //ServerConfigFile = new GS_ServerConfig();

            RetrieveSettings(ClientConfigFile);

        }

        public void RetrieveSettings(GS_ConfigFile cfg)
        {
            /*
            while (cfg.Input.BaseStream.CanRead)
            {
                char[] current = new char[128];
                char[] field = new char[64];
                char[] val = new char[64];

                // Find " = "
                current = cfg.Input.ReadLine().ToCharArray();
                for (int i = 0; i < current.Length; i++)
                {
                    if (current[i].Equals('='))
                    {
                        for (int j = 0; j < i; j++)
                            field[j] = current[j];

                        for (int k = i; k < current.Length; k++)
                            val[k - i] = current[k];
                    }
                }
                if (field.Length > 0 && val.Length > 0)
                    MasterSettingsList.Add(new GS_ConfigSetting(field.ToString(), val.ToString()));
            }
            cfg.Input.Close();
            */

        }

        public void Compile(GS_ConfigFile cfg)
        {
            cfg.OutputStream = new FileStream(
                cfg.GetValue("Config File"),
                FileMode.Create, FileAccess.Write);
            cfg.Output = new StreamWriter(cfg.OutputStream);

            foreach (GS_ConfigSetting setting in cfg.Settings)
            {
                cfg.Output.WriteLine(setting.Field + " = " + setting.Value);
            }
            cfg.Output.Close();
        }

    }
}