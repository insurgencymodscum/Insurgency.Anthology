﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GlobalStrike.Config
{
    public class GS_ConfigFile
    {
        public StreamReader Input;
        public StreamWriter Output;
        public FileStream InputStream;
        public FileStream OutputStream;

        public String FileName;
        public List<GS_ConfigSetting> Settings;

        public GS_ConfigFile()
        {
            Settings = new List<GS_ConfigSetting>();
        }

        public void AddSetting(GS_ConfigSetting setting)
        {
            bool edit = false;
            // Check if setting is already added
            foreach (GS_ConfigSetting cfg_setting in Settings)
            {
                if (cfg_setting.Field == setting.Field)
                {
                    cfg_setting.Value = setting.Value;
                    edit = true;
                }
            }
            if (!edit) Settings.Add(setting);
        }

        public void RemoveSetting(GS_ConfigSetting setting)
        {
            Settings.Remove(setting);
        }

        public void EditSetting(GS_ConfigSetting setting)
        {
            foreach (GS_ConfigSetting cfg_setting in Settings)
            {
                if (cfg_setting.Field == setting.Field)
                    cfg_setting.Value = setting.Value;
            }
        }

        public String GetValue(String field)
        {
            foreach (GS_ConfigSetting cfg_setting in Settings)
            {
                if (cfg_setting.Field == field)
                {
                    return cfg_setting.Value;
                }
            }
            return null;
        }


    }
}
