﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Input;
using GlobalStrike.Logger;
using GlobalStrike.Renderer;

namespace GlobalStrike.Config
{
    public class GS_ConfigSetting
    {
        protected String SettingField;
        protected String SettingValue;

        public GS_ConfigSetting(String field, String value)
        {
            SettingField = field;
            SettingValue = value;
        }

        public String Field
        {
            get { return SettingField; }
            set { SettingField = value; }
        }

        public String Value
        {
            get { return SettingValue; }
            set { SettingValue = value; }
        }
    }
}
