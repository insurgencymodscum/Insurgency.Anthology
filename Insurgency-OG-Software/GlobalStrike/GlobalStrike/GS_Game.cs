﻿#region Comments
/*
 * Global-Strike GSGame Class
 * by Jeremy Blum
*/
#endregion

#region Plan
/*   
*/
#endregion

#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Input;
using GlobalStrike.Config;
using GlobalStrike.Logger;
using GlobalStrike.HUD;
#endregion

namespace GlobalStrike
{
    public class GS_Game : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager DeviceManager;

        public GS_ConsoleMenu Console;
        public GS_Renderer GS_Graphics;
        public GS_InputManager InputManager;
        public GS_ConfigFileManager ConfigManager;

        public static bool BGActive = true;
        public static GS_FPS FPS;

        public int TotalPlayers;

        #region Constructor(s)

        //
        // GSGame()
        // Game constructor
        //
        public GS_Game()
        {
            GS_Log.addLine("LOG FILE BEGIN", "GS_Game.cs");

            // Set MP settings
            TotalPlayers = 3; 
            GS_Log.addLine("Total Players set to " + TotalPlayers, "GS_Game.cs");

            // Create Renderer
            Content.RootDirectory = "Content";
            GS_Log.addLine("Content directory set to " + Content.RootDirectory, "GS_Game.cs");

            // Create Console
            FPS = new GS_FPS(this);
            GS_Log.addLine("FPS Determined", "GS_Game.cs");

            //Batch = new SpriteBatch(GraphicsDevice.);
            DeviceManager = new GraphicsDeviceManager(this);
            DeviceManager.PreferredBackBufferWidth = 400;
            DeviceManager.PreferredBackBufferHeight = 300;
            DeviceManager.ApplyChanges();

            Console = new GS_ConsoleMenu(this);
            GS_Graphics = new GS_Renderer(this);
            InputManager = new GS_PC_InputManager(this);
            //ConfigManager = new GS_ConfigFileManager(this);

        }

        #endregion

        #region Initialization and Loading

        //
        // Initialize()
        // Perform actions before the game is running
        //
        protected override void Initialize()
        {
            Components.Add(Console);
            GS_Log.addNewLine("Console Component Added", "GS_Game.cs");

            Components.Add(GS_Graphics);
            GS_Log.addLine("Renderer Component Added", "GS_Game.cs");

            Components.Add(InputManager);
            GS_Log.addLine("Input Component Added", "GS_Game.cs");

            Components.Add(ConfigManager);
            GS_Log.addLine("Configuration Component Added", "GS_Game.cs");

            GS_Log.addLine("Screen size Set", "GS_Game.cs");

            base.Initialize();
        }

        protected override void LoadContent()
        {
        }

        protected override void UnloadContent()
        {

            // Unload non ContentManager content here
            Content.Unload();
            GS_Log.addNewLine("Content successfully unloaded", "GS_Game.cs");

            GS_Log.addNewLine("LOG FILE END", "GS_Game.cs");
            GS_Log.compile(new FileStream(GS_Log.path,
                FileMode.Create, FileAccess.Write));
        }

        #endregion

        #region Update and Draw

        //
        // Update()
        // Method called at every change in gameTime to determine game
        // logic before Draw is called
        //
        protected override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }

        //
        // Draw()
        // Called after Update() to render the content in the game
        //
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        #endregion
    }
}
