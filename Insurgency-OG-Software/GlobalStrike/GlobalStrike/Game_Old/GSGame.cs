


using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace GlobalStrike
{
    public class GSGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager Renderer;
        SpriteBatch UserInterface;
        Scene3D World_Models;

        public GameTime Time;
        int TotalPlayers;

        public Controller Player1 { get; protected set; }
        public Controller Player2 { get; protected set; }
        public Controller Player3 { get; protected set; }
        public Controller Player4 { get; protected set; }
        public Controller Controlled_Player { get; protected set; }

        public Eye Player1_Perspective { get; protected set; }
        public Eye Player2_Perspective { get; protected set; }
        public Eye Player3_Perspective { get; protected set; }
        public Eye Player4_Perspective { get; protected set; }
        public Eye Controlled_Perspective { get; protected set; }
        public Eye Current_Perspective { get; protected set; }

        public Viewport Player1_Viewport;
        public Viewport Player2_Viewport;
        public Viewport Player3_Viewport;
        public Viewport Player4_Viewport;
        public Viewport Controlled_Viewport;

        public void SetControllableViewport(int vp)
        {
            switch (vp)
            {
                case 1:
                    Controlled_Viewport = Player1_Viewport;
                    Controlled_Perspective = Player1_Perspective;
                    break;
                case 2:
                    Controlled_Viewport = Player2_Viewport;
                    Controlled_Perspective = Player2_Perspective;
                    break;
                case 3:
                    Controlled_Viewport = Player3_Viewport;
                    Controlled_Perspective = Player3_Perspective;
                    break;
                case 4:
                    Controlled_Viewport = Player4_Viewport;
                    Controlled_Perspective = Player4_Perspective;
                    break;
            }
            Controlled_Perspective.AddCrosshair();
        }

        //
        // GSGame()
        // Game constructor
        //
        public GSGame()
        {
            Time = new GameTime();
            Renderer = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Renderer.PreferredBackBufferWidth = 500;
            Renderer.PreferredBackBufferHeight = 300;            
        }

        //
        // Initialize()
        // Perform actions before the game is running
        //
        protected override void Initialize()
        {
            // Set MP settings
            TotalPlayers = 1;

            // Create viewports
            int border = 1;

            switch (TotalPlayers)
            {
                case 1:
                    Player1_Viewport = GraphicsDevice.Viewport;
                    Player1_Viewport.Width = GraphicsDevice.Viewport.Width - (border*2);
                    Player1_Viewport.Height = GraphicsDevice.Viewport.Height - (border * 2);
                    Player1_Viewport.X = border;
                    Player1_Viewport.Y = border;
                    Player1_Perspective = new Eye(this, new Vector3(0, 0, 100),
                        Vector3.Zero, Vector3.Up, Player1_Viewport);
                    Components.Add(Player1_Perspective);                    
                    break;
                case 2:
                    Player1_Viewport = GraphicsDevice.Viewport;
                    Player1_Viewport.X = border;
                    Player1_Viewport.Y = border;
                    Player1_Viewport.Width = GraphicsDevice.Viewport.Width - (border * 2);
                    Player1_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - border;
                    Player1_Perspective = new Eye(  this, new Vector3(0, 0, -50),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player1_Viewport);
                    Components.Add(Player1_Perspective);

                    Player2_Viewport = GraphicsDevice.Viewport;
                    Player2_Viewport.X = border;
                    Player2_Viewport.Y = GraphicsDevice.Viewport.Height / 2 + 1;
                    Player2_Viewport.Width = GraphicsDevice.Viewport.Width - (border * 2);
                    Player2_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - (border*2);
                    Player2_Perspective = new Eye(this, new Vector3(0, 0, 50),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player2_Viewport);
                    Components.Add(Player2_Perspective);
                    break;
                case 3:
                    Player1_Viewport = GraphicsDevice.Viewport;
                    Player1_Viewport.X = border;
                    Player1_Viewport.Y = border;
                    Player1_Viewport.Width = GraphicsDevice.Viewport.Width / 2 - border;
                    Player1_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - border;
                    Player1_Perspective = new Eye(this, new Vector3(0, 0, -100),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player1_Viewport);
                    Components.Add(Player1_Perspective);

                    Player2_Viewport = GraphicsDevice.Viewport;
                    Player2_Viewport.X = GraphicsDevice.Viewport.Width / 2 + border;
                    Player2_Viewport.Y = border;
                    Player2_Viewport.Width = GraphicsDevice.Viewport.Width / 2 - (border * 2);
                    Player2_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - border;
                    Player2_Perspective = new Eye(this, new Vector3(0, 0, 0),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player2_Viewport);
                    Components.Add(Player2_Perspective);

                    Player3_Viewport = GraphicsDevice.Viewport;
                    Player3_Viewport.X = border;
                    Player3_Viewport.Y = GraphicsDevice.Viewport.Height / 2 + border;
                    Player3_Viewport.Width = GraphicsDevice.Viewport.Width - (border*2);
                    Player3_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - (border*2);
                    Player3_Perspective = new Eye(this, new Vector3(0, 0, 100),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player3_Viewport);
                    Components.Add(Player3_Perspective);
                    break;
                case 4:
                    Player1_Viewport = GraphicsDevice.Viewport;
                    Player1_Viewport.X = border;
                    Player1_Viewport.Y = border;
                    Player1_Viewport.Width = GraphicsDevice.Viewport.Width / 2 - border;
                    Player1_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - border;
                    Player1_Perspective = new Eye(this, new Vector3(0, 0, -150),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player1_Viewport);
                    Components.Add(Player1_Perspective);

                    Player2_Viewport = GraphicsDevice.Viewport;
                    Player2_Viewport.X = GraphicsDevice.Viewport.Width / 2 + border;
                    Player2_Viewport.Y = border;
                    Player2_Viewport.Width = GraphicsDevice.Viewport.Width / 2 - (border*2);
                    Player2_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - border;
                    Player2_Perspective = new Eye(this, new Vector3(0, 0, -50),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player2_Viewport);
                    Components.Add(Player2_Perspective);

                    Player3_Viewport = GraphicsDevice.Viewport;
                    Player3_Viewport.X = border;
                    Player3_Viewport.Y = GraphicsDevice.Viewport.Height / 2 + border;
                    Player3_Viewport.Width = GraphicsDevice.Viewport.Width / 2 - border;
                    Player3_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - (border*2);
                    Player3_Perspective = new Eye(this, new Vector3(0, 0, 50),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player3_Viewport);
                    Components.Add(Player3_Perspective);

                    Player4_Viewport = GraphicsDevice.Viewport;
                    Player4_Viewport.X = GraphicsDevice.Viewport.Width / 2 + border;
                    Player4_Viewport.Y = GraphicsDevice.Viewport.Height / 2 + border;
                    Player4_Viewport.Width = GraphicsDevice.Viewport.Width / 2 - (border * 2);
                    Player4_Viewport.Height = GraphicsDevice.Viewport.Height / 2 - (border*2);
                    Player4_Perspective = new Eye(this, new Vector3(0, 0, 150),
                                                    Vector3.Zero, Vector3.Up,
                                                    Player4_Viewport);
                    Components.Add(Player4_Perspective);
                    break;

            }

            //Initialize model manager
            World_Models = new Scene3D(this);
            Components.Add(World_Models);

            SetControllableViewport(1);

            base.Initialize();
        }

        //
        // LoadContent()
        // Load pre-required game content
        //
        protected override void LoadContent()
        {
            UserInterface = new SpriteBatch(GraphicsDevice);

            // Load game content using this.Content
        }
        
        //
        // UnloadContent()
        // Unload all content
        //
        protected override void UnloadContent()
        {
            // Unload non ContentManager content here
        }

        //
        // Update()
        // Method called at every change in gameTime to determine game
        // logic before Draw is called
        //
        protected override void Update(GameTime gameTime)
        {
            CheckInputs();

            base.Update(gameTime);
        }

        void CheckInputs()
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            Controlled_Perspective.CheckPositionChange();
            Controlled_Perspective.CheckRotationChange();
        }

        //
        // Draw()
        // Called after Update() to render the content in the game
        //
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear( Color.Black );

            if (TotalPlayers >= 1)
                Current_Perspective = Player1_Perspective;
                GraphicsDevice.Viewport = Player1_Viewport;
                GraphicsDevice.Clear(Color.CornflowerBlue);
                base.Draw(gameTime);

            if (TotalPlayers >= 2)
                Current_Perspective = Player2_Perspective;
                GraphicsDevice.Viewport = Player2_Viewport;
                GraphicsDevice.Clear(Color.CornflowerBlue);
                base.Draw(gameTime);

            if (TotalPlayers >= 3)
                Current_Perspective = Player3_Perspective;
                GraphicsDevice.Viewport = Player3_Viewport;
                GraphicsDevice.Clear(Color.CornflowerBlue);
                base.Draw(gameTime);

            if (TotalPlayers >= 4)
                Current_Perspective = Player4_Perspective;
                GraphicsDevice.Viewport = Player4_Viewport;
                GraphicsDevice.Clear(Color.CornflowerBlue);
                base.Draw(gameTime);
        }
    }
}
