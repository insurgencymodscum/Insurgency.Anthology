﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace GlobalStrike
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Eye : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch UserInterface;
        List<Object2D> Materials = new List<Object2D>();
        CrosshairSprite Crosshair;

        //Camera matrices
        public Matrix Camera { get; protected set; }
        public Matrix Projection { get; protected set; }
        public Viewport View { get; protected set; }
        public Rectangle Bounds { get; protected set; }

        // Camera vectors
        public Vector3 Position { get; protected set; }
        public Vector3 Direction;
        public Vector3 Up;

        //Speed of the camera
        public float Speed = 3;

        //Rotation
        MouseState prevMouseState;


        public Eye(Game game, Vector3 pos, Vector3 target, Vector3 up, Viewport view)
            : base(game)
        {
            // Build camera view matrix
            Position = pos;
            Direction = target - pos;
            Direction.Normalize();
            Up = up;
            CreateLookAt();

            Projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4,
                (float)view.Width /
                (float)view.Height,
                1, 3000);

            View = view;
            Bounds = new Rectangle(View.X, View.Y, View.Width, View.Height);
        }

        public void AddCrosshair()
        {
            Vector2 MiddleScreen = new Vector2(Bounds.Width / 2, Bounds.Height / 2);
            Crosshair = new CrosshairSprite(
                Game.Content.Load<Texture2D>(@"Images/cross"),
                MiddleScreen,
                new Point(128, 128), 64, new Point(0, 0), new Point(6, 8),
                new Vector2(6, 6));
            Materials.Add(Crosshair);
        }

        public bool hasCrosshair()
        {
            if (Crosshair != null) return true;
            else return false;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            /*
            HUD = new Scene2D(Game);
            Game.Components.Add(HUD);
            */

            // Set mouse position and do initial get state
            Mouse.SetPosition(Game.Window.ClientBounds.Width / 2,
            Game.Window.ClientBounds.Height / 4);
            prevMouseState = Mouse.GetState();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            UserInterface = new SpriteBatch(Game.GraphicsDevice);

            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {    
            // Recreate the camera view matrix
            CreateLookAt();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            UserInterface.Begin(
                SpriteBlendMode.AlphaBlend,
                SpriteSortMode.FrontToBack,
                SaveStateMode.None);

            
            foreach (Object2D mat in Materials)
                mat.Draw(gameTime, UserInterface);
            
            UserInterface.End();

            base.Draw(gameTime);
        }

        public void CheckPositionChange()
        {
            // Move forward/backward
            if (Keyboard.GetState().IsKeyDown(Keys.W))
                Position += Direction * Speed;
            if (Keyboard.GetState().IsKeyDown(Keys.S))
                Position -= Direction * Speed;

            // Move side to side
            if (Keyboard.GetState().IsKeyDown(Keys.A))
                Position += Vector3.Cross(Up, Direction) * Speed;
            if (Keyboard.GetState().IsKeyDown(Keys.D))
                Position -= Vector3.Cross(Up, Direction) * Speed;
        }

        public void CheckRotationChange()
        {
            // Yaw rotation
            Direction = Vector3.Transform(Direction,
            Matrix.CreateFromAxisAngle(Up, (-MathHelper.PiOver4 / 150) *
            (Mouse.GetState().X - prevMouseState.X)));

            // Roll rotation
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                Up = Vector3.Transform(Up,
                Matrix.CreateFromAxisAngle(Direction,
                MathHelper.PiOver4 / 45));
            }
            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                Up = Vector3.Transform(Up,
                Matrix.CreateFromAxisAngle(Direction,
                -MathHelper.PiOver4 / 45));
            }

            // Pitch rotation
            Direction = Vector3.Transform(Direction,
                Matrix.CreateFromAxisAngle(Vector3.Cross(Up, Direction),
                (MathHelper.PiOver4 / 100) *
                (Mouse.GetState().Y - prevMouseState.Y)));

            Up = Vector3.Transform(Up,
                Matrix.CreateFromAxisAngle(Vector3.Cross(Up, Direction),
                (MathHelper.PiOver4 / 100) *
                (Mouse.GetState().Y - prevMouseState.Y)));

            if (prevMouseState.X != Mouse.GetState().X || prevMouseState.Y != Mouse.GetState().Y)
            {
                int new_x = Mouse.GetState().X;
                int new_y = Mouse.GetState().Y;
                if (Mouse.GetState().X < Bounds.X) new_x = Bounds.X;
                if (Mouse.GetState().X > Bounds.X + Bounds.Width) new_x = Bounds.X + Bounds.Width;
                if (Mouse.GetState().Y < Bounds.Y) new_y = Bounds.Y;
                if (Mouse.GetState().Y > Bounds.Y + Bounds.Height) new_y = Bounds.Y + Bounds.Height;
                Mouse.SetPosition(new_x, new_y);
            }

            // Reset prevMouseState
            prevMouseState = Mouse.GetState();
        }

        private void CreateLookAt()
        {
            Camera = Matrix.CreateLookAt(Position, Position + Direction, Up);
        }
    }
}