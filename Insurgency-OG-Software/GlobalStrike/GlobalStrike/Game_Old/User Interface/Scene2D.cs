//
// Global-Strike SpriteManager Class
// by Jeremy Blum
//

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


namespace GlobalStrike
{
    public class Scene2D : DrawableGameComponent
    {
        SpriteBatch UserInterface;
        CrosshairSprite Crosshair;

        public void setCrosshairPosition(int x, int y)
        {
            Crosshair.SetPosition(x, y);
        }

        List<Object2D> Materials = new List<Object2D>();

        public Scene2D(Game game)
            : base(game)
        {
            Crosshair = new CrosshairSprite(
                Game.Content.Load<Texture2D>(@"Images/cross"),
                new Vector2(Mouse.GetState().X, Mouse.GetState().Y),
                new Point(64, 64), 10, new Point(0, 0), new Point(6, 8),
                new Vector2(6, 6)); 
            
            Materials.Add((Object2D)Crosshair);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            UserInterface = new SpriteBatch(Game.GraphicsDevice);            

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            UserInterface.Begin(
                SpriteBlendMode.AlphaBlend,
                SpriteSortMode.FrontToBack,
                SaveStateMode.None);

            /*
            foreach (Object2D mat in Materials)
                mat.Draw(gameTime, UserInterface);
            */

            Crosshair.Draw(gameTime, UserInterface);
            UserInterface.End();
            
            base.Draw(gameTime);
        }
    }
}