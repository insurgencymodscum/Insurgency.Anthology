﻿//
// Global-Strike Sprite Class
// by Jeremy Blum
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GlobalStrike
{
    abstract class Object2D
    {
        // Variables
        protected Texture2D textureImage;
        protected Vector2 position;
        protected Point frameSize;
        int collisionOffset;
        Point currentFrame;
        Point sheetSize;
        int timeSinceLastFrame = 0;
        int millisecondsPerFrame;
        protected Vector2 speed;
        const int defaultMillisecondsPerFrame = 16;


        public void SetPosition(int x, int y)
        {
            position = new Vector2((float)x, (float)y);
        }

        //
        // Sprite Constructor
        //
        public Object2D(
            Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset,
            Point currentFrame, Point sheetSize,
            Vector2 speed ) 
            : this (
                textureImage, position, frameSize,
                collisionOffset, currentFrame, sheetSize,
                speed, defaultMillisecondsPerFrame ) 
        {
        }

        public Object2D(
            Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset,
            Point currentFrame, Point sheetSize,
            Vector2 speed, int millisecondsPerFrame ) 
        {
            this.textureImage = textureImage;
            this.position = position;
            this.frameSize = frameSize;
            this.collisionOffset = collisionOffset;
            this.currentFrame = currentFrame;
            this.sheetSize = sheetSize;
            this.speed = speed;
            this.millisecondsPerFrame = millisecondsPerFrame;
        }

        public virtual void Update(GameTime gameTime, Rectangle clientBounds)
        {
            timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastFrame > millisecondsPerFrame)
            {
                timeSinceLastFrame = 0;
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.X)
                        currentFrame.Y = 0;
                }
            }

        }

        public virtual void Draw(GameTime gameTime, SpriteBatch ui)
        {
            ui.Draw(
                textureImage, position,
                new Rectangle(currentFrame.X * frameSize.X,
                    currentFrame.Y * frameSize.Y,
                    frameSize.X, frameSize.Y),
                Color.White, 0, 
                new Vector2((float)(textureImage.Width / 2),
                            (float)(textureImage.Height / 2)),
                1f, SpriteEffects.None, 0);
        }

        public abstract Vector2 direction { get; }

        public Rectangle collisionRect
        {
            get
            {
                return new Rectangle(
                    (int)position.X + collisionOffset,
                    (int)position.Y + collisionOffset,
                    frameSize.X - (collisionOffset * 2),
                    frameSize.Y - (collisionOffset * 2));
            }
        }
    }
}
