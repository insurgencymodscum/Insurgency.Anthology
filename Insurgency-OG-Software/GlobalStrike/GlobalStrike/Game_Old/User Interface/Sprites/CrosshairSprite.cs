﻿//
// Global-Strike UserControlledSprite Class
// by Jeremy Blum
//

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GlobalStrike
{
    class CrosshairSprite : Object2D
    {
        // Variables
        MouseState prevMouseState;

        public CrosshairSprite(
            Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset,
            Point currentFrame, Point sheetSize,
            Vector2 speed )
            : base(
                textureImage, position, frameSize,
                collisionOffset, currentFrame,
                sheetSize, speed )
        {
        }

        public CrosshairSprite(
            Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset,
            Point currentFrame, Point sheetSize,
            Vector2 speed, int millisecondsPerFrame)
            : base(
                textureImage, position, frameSize,
                collisionOffset, currentFrame,
                sheetSize, speed, millisecondsPerFrame)
        {
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            int width = textureImage.Width;
            int height = textureImage.Height;
            /*
            if (Mouse.GetState().X != prevMouseState.X)
                position.X = Mouse.GetState().X;
            if (Mouse.GetState().Y != prevMouseState.Y)
                position.Y = Mouse.GetState().Y;
            */
            prevMouseState = Mouse.GetState();
            
            /*
            if (timeSinceLastFrame > millisecondsPerFrame)
            {
                timeSinceLastFrame = 0;
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.X)
                        currentFrame.Y = 0;
                }
            }
            */
        }

        public override Vector2 direction
        {
            get { return speed; }
        }
    }
}

/*
namespace GlobalStrike
{
    class CrosshairSprite : Sprite
    {
        MouseState prevMouseState;

        public CrosshairSprite(
            Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset,
            Point currentFrame, Point sheetSize,
            Vector2 speed)
            : base(
                textureImage, position, frameSize,
                collisionOffset, currentFrame,
                sheetSize, speed)
        {
        }

        public CrosshairSprite(
            Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset,
            Point currentFrame, Point sheetSize,
            Vector2 speed, int millisecondsPerFrame)
            : base(
                textureImage, position, frameSize,
                collisionOffset, currentFrame,
                sheetSize, speed, millisecondsPerFrame)
        {
        }

        public override Vector2 direction
        {
            get
            {
                Vector2 inputDirection = Vector2.Zero;

                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                    inputDirection.X -= 1;
                if (Keyboard.GetState().IsKeyDown(Keys.Right))
                    inputDirection.X += 1;
                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                    inputDirection.Y -= 1;
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                    inputDirection.Y += 1;

                GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
                if (gamepadState.ThumbSticks.Left.X != 0)
                    inputDirection.X += gamepadState.ThumbSticks.Left.X;
                if (gamepadState.ThumbSticks.Left.Y != 0)
                    inputDirection.Y += gamepadState.ThumbSticks.Left.Y;

                return inputDirection * speed;
            }
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            // Move the sprite according to the direction property
            position += direction;

            // If the mouse moved, set the position of the sprite to the mouse position
            MouseState currMouseState = Mouse.GetState();
            if (currMouseState.X != prevMouseState.X ||
                currMouseState.Y != prevMouseState.Y)
            {
                position = new Vector2(currMouseState.X, currMouseState.Y);
            }
            prevMouseState = currMouseState;

            // If the sprite is off the screen, put it back in play
            if (position.X < 0) position.X = 0;
            if (position.Y < 0) position.Y = 0;
            if (position.X > clientBounds.Width - frameSize.X)
                position.X = clientBounds.Width - frameSize.X;
            if (position.Y > clientBounds.Height - frameSize.Y)
                position.Y = clientBounds.Height - frameSize.Y;

            base.Update(gameTime, clientBounds);
        }
    }
}
*/