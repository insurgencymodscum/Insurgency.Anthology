﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GlobalStrike
{
    class Object3D
    {
        public Model Data { get; protected set; }
        protected Matrix Position = Matrix.Identity;

        public Object3D(Model m)
        {
            Data = m;
        }

        public virtual void Update()
        {
            //Base class does nothing here
        }

        public void Draw(Eye camera)
        {
            //Set transfroms
            Matrix[] transforms = new Matrix[Data.Bones.Count];
            Data.CopyAbsoluteBoneTransformsTo(transforms);

            //Loop through meshes and their effects 
            foreach (ModelMesh mesh in Data.Meshes)
            {
                foreach (BasicEffect be in mesh.Effects)
                {
                    //Set BasicEffect information
                    be.EnableDefaultLighting();
                    be.Projection = camera.Projection;
                    be.View = camera.Camera;
                    be.World = GetPosition() * mesh.ParentBone.Transform;
                }
                //Draw
                mesh.Draw();
            }
        }

        public virtual Matrix GetPosition()
        {
            return Position;
        }

    }
}
