﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.Logger;

namespace GlobalStrike.Renderer.Scene
{
    public class GS_Model
    {
        public Model Data { get; protected set; }
        protected Matrix matrix;
        protected Matrix[] transforms;

        protected Vector3 translate;
        protected Vector3 rotate;
        protected Vector3 scale;
        protected bool needUpdate;

        public GS_Model(Model m)
        {
            GS_Log.addNewLine("Creating model", "GS_Model.cs");
            Data = m;

            // Set Matrix
            matrix = Matrix.Identity;

            // Set transforms
            transforms = new Matrix[Data.Bones.Count];
            Data.CopyAbsoluteBoneTransformsTo(transforms);

            DisplayData(Data);
        }

        public Vector3 Translate
        {
            get { return translate; }
            set { translate = value; needUpdate = true; }
        }
        public Vector3 Rotate
        {
            get { return rotate; }
            set { rotate = value; needUpdate = true; }
        }
        public Vector3 Scale
        {
            get { return scale; }
            set { scale = value; needUpdate = true; }
        }

        public Matrix Matrix
        {
            get
            {
                if (needUpdate)
                {
                    // Compute the final matrix (Scale * Rotate * Translate)
                    matrix = Matrix.CreateScale(scale) *
                    Matrix.CreateRotationY(MathHelper.ToRadians(rotate.Y)) *
                    Matrix.CreateRotationX(MathHelper.ToRadians(rotate.X)) *
                    Matrix.CreateRotationZ(MathHelper.ToRadians(rotate.Z)) *
                    Matrix.CreateTranslation(translate);
                    needUpdate = false;
                }
                return matrix;
            }
        }

        public void DisplayData(Model m)
        {
            GS_Log.addNewLine("======================================", "GS_Model.cs");
            GS_Log.addNewLine("Displaying Data for Model:", "GS_Model.cs");

            String meshes = "Meshes: ";
            foreach (ModelMesh mesh in m.Meshes)
            {
                meshes += Environment.NewLine + GS_Log.JumpTo(0, GS_Log.COL2) + mesh.Name + " (";

                foreach (ModelMeshPart part in mesh.MeshParts)
                    meshes += part.NumVertices + " Vertices)" ;
            }
            GS_Log.addNewLine(meshes, "GS_Model.cs");

            String bones = "Bones: ";
            foreach (ModelBone bone in m.Bones)
                bones += Environment.NewLine + GS_Log.JumpTo(0, GS_Log.COL2) + bone.Name;
            GS_Log.addNewLine(bones, "GS_Model.cs");

            GS_Log.addNewLine("======================================", "GS_Model.cs");

        }

        public virtual void Update()
        {
            //Base class does nothing here
        }

        public void Draw(GS_FP_Camera camera)
        {

            //Loop through meshes and their effects 
            foreach (ModelMesh mesh in Data.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //Set BasicEffect information
                    effect.EnableDefaultLighting();
                    effect.Projection = camera.ProjectionMatrix;
                    effect.View = camera.ViewMatrix;
                    effect.World = GetMatrix() * mesh.ParentBone.Transform;
                }
                //Draw
                mesh.Draw();
            }
        }

        public virtual Matrix GetMatrix()
        {
            return Matrix;
        }

    }
}
