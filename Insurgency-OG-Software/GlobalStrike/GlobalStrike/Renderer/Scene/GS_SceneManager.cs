﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.HUD;

namespace GlobalStrike.Renderer.Scene
{
    public class GS_SceneManager : GameComponent
    {
        public List<GS_Model> Models;
        List<GS_Camera> Cameras;
        public GS_HUD HUD;
        GS_Model Env;
        GS_Model Ship;

        public GS_SceneManager(GS_Game game, GS_Renderer renderer)
            : base(game)
        {
            Models = new List<GS_Model>();
            HUD = new GS_HUD(game, renderer);
            //Cameras = renderer.ViewportManager.ViewportCameras;
        }

        public void AddModel(GS_Model m)
        {
            Models.Add(m);
        }

        public override void Initialize()
        {
            GS_HUD_Item crosshair = new GS_HUD_Item( Game.Content.Load<Texture2D>(@"Images/crosshair"));
            HUD.AddItem(crosshair);

            Env = new GS_Model(Game.Content.Load<Model>(@"Maps/test_enivronment"));
            Env.Translate = new Vector3(-30, 100, -50);
            Env.Scale = new Vector3(4.0f, 4.0f, 4.0f);
            AddModel(Env);

            Ship = new GS_Model(Game.Content.Load<Model>(@"Models/spaceship"));
            Ship.Translate = new Vector3(-30, -40, -100); // X, Y, Z
            Ship.Rotate = new Vector3(20, -163, -5); // Pitch, Yaw, Roll
            Ship.Scale = new Vector3(3.0f, 3.0f, 3.0f);
            AddModel(Ship);

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            // Loop through all models and call Update
            for (int i = 0; i < Models.Count; ++i)
            {
                Models[i].Update();
            }

            base.Update(gameTime);
        }
    }
}