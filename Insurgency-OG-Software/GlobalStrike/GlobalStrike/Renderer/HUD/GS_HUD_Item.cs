﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Logger;

namespace GlobalStrike.HUD
{
    public class GS_HUD_Item
    {
        protected Texture2D texture;
        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        protected Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        protected Rectangle collision;
        public Rectangle Collision
        {
            get
            {
                collision = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
                return collision;
            }
            set { collision = value; }
        }

        protected float scale;
        public float Scale
        {
            get { return scale; }
            set 
            { 
                scale = value; 
            }
        }

        protected Color color;
        public Color Diffuse_Color
        {
            get { return color; }
            set { color = value; }
        }

        public GS_HUD_Item(Texture2D tex)
        {
            Texture = tex;
            Position = new Vector2(0, 0);
            Scale = 1.0f;
            Diffuse_Color = Color.White;
        }
    }
}
