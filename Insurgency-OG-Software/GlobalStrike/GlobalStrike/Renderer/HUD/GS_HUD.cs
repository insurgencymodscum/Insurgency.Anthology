﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.Renderer.Scene;

namespace GlobalStrike.HUD
{
    public class GS_HUD : GameComponent
    {
        public List<GS_HUD_Item> Items;
        List<GS_FP_Camera> Cameras;

        public GS_HUD(GS_Game game, GS_Renderer renderer) : base(game)
        {
            Items = new List<GS_HUD_Item>();
        }

        public void ApplyCameras(List<GS_FP_Camera> cams)
        {
            Cameras = cams;
        }

        public void AddItem(GS_HUD_Item item)
        {
            Items.Add(item);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}