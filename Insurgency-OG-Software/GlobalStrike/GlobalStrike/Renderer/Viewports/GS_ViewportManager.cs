﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Input;
using GlobalStrike.Logger;
using GlobalStrike.HUD;
#endregion

namespace GlobalStrike.Renderer.Viewports
{
    public class GS_ViewportManager : DrawableGameComponent
    {
        #region Data

        protected Viewport Window;
        protected int ControlledViewport;
        protected GS_PC_InputManager InputManager;

        public List<GS_Viewport> Viewports;
        public List<GS_FP_Camera> ViewportCameras;

        public GS_HUD HUDManager;

        int TotalPlayers;
        int border;


        #endregion

        #region Initialization

        public GS_ViewportManager(GS_Game game, GS_Renderer renderer, int players) : base(game)
        {
            Window = game.DeviceManager.GraphicsDevice.Viewport;
            Viewports = new List<GS_Viewport>();
            ViewportCameras = new List<GS_FP_Camera>();
            TotalPlayers = players;
            border = 1;
            InputManager = new GS_PC_InputManager(game);
            HUDManager = new GS_HUD(game, renderer);
            GS_Log.addNewLine("New Viewport Manager Created", "GS_ViewportManager.cs");
        }

        public override void Initialize()
        {
            Game.Components.Add(InputManager);

            GS_Log.addNewLine("Window Properties: X=" + Window.X + ", Y=" + Window.Y + ", Width=" + Window.Width + ", Height=" + Window.Height, "GS_ViewportManager");
            switch (TotalPlayers)
            {
                case 1:

                    AddViewport(new GS_Viewport(
                        Window.Width - (border * 2),
                        Window.Height - (border * 2),
                        border, border));

                    AddCamera(new GS_FP_Camera(new Vector3(0,0,0),
                        new Vector3(-30, -30, -100),
                        GetCurrentViewport()));

                    ControlledViewport = 1;

                    break;
                
                case 2:

                    AddViewport(new GS_Viewport(
                        Window.Width - (border * 2),
                        Window.Height / 2 - border,
                        border, border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, -50),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    AddViewport(new GS_Viewport(
                        Window.Width - (border * 2),
                        Window.Height / 2 - (border * 2),
                        border, Window.Height / 2 + 1));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, 50),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    ControlledViewport = 1;

                    break;

                case 3:
                    AddViewport(new GS_Viewport(
                        Window.Width / 2 - border,
                        Window.Height / 2 - border,
                        border, border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, -100),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    AddViewport(new GS_Viewport(
                        Window.Width / 2 - (border * 2),
                        Window.Height / 2 - border,
                        Window.Width / 2 + border, border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, 0),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    AddViewport(new GS_Viewport(
                        Window.Width - (border * 2),
                        Window.Height / 2 - (border * 2),
                        border, Window.Height / 2 + border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, 100),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    ControlledViewport = 1;

                    break;

                case 4:

                    AddViewport(new GS_Viewport(
                        Window.Width / 2 - border,
                        Window.Height / 2 - border,
                        border, border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, -150),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    AddViewport(new GS_Viewport(
                        Window.Width / 2 - (border * 2),
                        Window.Height / 2 - border,
                        Window.Width / 2 + border, border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, -50),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    AddViewport(new GS_Viewport(
                        Window.Width / 2 - border,
                        Window.Height / 2 - (border * 2),
                        border, Window.Height / 2 + border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, 50),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    AddViewport(new GS_Viewport(
                        Window.Width / 2 - (border * 2),
                        Window.Height / 2 - (border * 2),
                        Window.Width / 2 + border, Window.Height / 2 + border));

                    AddCamera(new GS_FP_Camera(
                        new Vector3(0, 0, 150),
                        Vector3.Zero, 
                        GetCurrentViewport()));

                    ControlledViewport = 1;

                    break;
                 
            }

            HUDManager.ApplyCameras(ViewportCameras);

            Game.Components.Add(HUDManager);
            GS_Log.addLine("HUD Manager Component Added", "GS_Renderer.cs");

            base.Initialize();
        }

        #endregion

        #region Functions

        public GS_Viewport GetControlledViewport()
        {
            return Viewports.ElementAt<GS_Viewport>(ControlledViewport - 1);
        }

        public GS_FP_Camera GetControlledCamera()
        {
            foreach (GS_FP_Camera cam in ViewportCameras)
                if (cam.View == GetControlledViewport())
                    return cam;
            return null;

        }

        public void AddViewport(GS_Viewport view)
        {
            Viewports.Add(view);
            int index = Viewports.IndexOf(view);
            GS_Log.addNewLine("Adding New Viewport:", "GS_ViewportManager.cs");
            GS_Log.addLine("\t\t\t\tIndex: " + index, "GS_ViewportManager.cs");
            GS_Log.addLine("\t\t\t\tX: " + view.GetX(), "GS_ViewportManager.cs");
            GS_Log.addLine("\t\t\t\tY: " + view.GetY(), "GS_ViewportManager.cs");
            GS_Log.addLine("\t\t\t\tWidth: " + view.GetWidth(), "GS_ViewportManager.cs");
            GS_Log.addLine("\t\t\t\tHeight: " + view.GetHeight(), "GS_ViewportManager.cs");
        }

        public void RemoveViewport(GS_Viewport view)
        {
            int index = Viewports.IndexOf(view);
            Viewports.Remove(view);
            GS_Log.addNewLine("Removing Viewport #" + index, "GS_ViewportManager.cs");
        }

        public GS_Viewport GetViewport(int i)
        {
            return Viewports.ElementAt<GS_Viewport>(i);
        }

        public GS_Viewport GetCurrentViewport()
        {
            return Viewports.ElementAt<GS_Viewport>( Viewports.Count-1);
        }

        public void AddCamera(GS_FP_Camera cam)
        {
            ViewportCameras.Add(cam);
            int index = ViewportCameras.IndexOf(cam);
            GS_Log.addNewLine("Adding Camera #" + index + " for Viewport #" + Viewports.IndexOf(cam.View), "GS_ViewportManager.cs");
        }

        public void RemoveCamera(GS_FP_Camera cam)
        {
            int index = ViewportCameras.IndexOf(cam);
            ViewportCameras.Remove(cam);
            GS_Log.addLine("Camera #" + index + " Removed", "GS_ViewportManager.cs");
        }

        public GS_FP_Camera GetCamera(int i)
        {
            return ViewportCameras.ElementAt<GS_FP_Camera>(i);
        }

        public GS_FP_Camera GetCurrentCamera()
        {
            return ViewportCameras.ElementAt<GS_FP_Camera>(ViewportCameras.Count-1);
        }

        #endregion

        #region Update

        protected void Look(GS_FP_Camera cam, Vector3 velocity)
        {

        }


        public override void Update(GameTime gameTime)
        {

            if (GetControlledViewport() != null && GetControlledCamera() != null)
            {
                InputManager.DetectMouseMovement(GetControlledCamera());
                InputManager.DetectKeyboardAction(GetControlledCamera());
            }

            InputManager.prevMouseState = Mouse.GetState();

            base.Update(gameTime);
        }

        #endregion
    }
}