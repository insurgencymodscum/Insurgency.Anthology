﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Input;
#endregion

namespace GlobalStrike.Renderer.Viewports
{
    public class GS_Viewport
    {
        #region Data
        Viewport Window;
        public Viewport View;
        int border;
        
        #endregion

        #region Initialization

        public GS_Viewport()
        {
            View = new Viewport();
        }

        public GS_Viewport(int width, int height, int x, int y)
        {
            border = 1;

            View = new Viewport();
            View.Width = width;
            View.Height = height;
            View.X = x;
            View.Y = y;
        }

        #endregion 

        #region Functions

        public int GetWidth()
        {
            return View.Width;
        }

        public int GetHeight()
        {
            return View.Height;
        }

        public int GetX()
        {
            return View.X;
        }

        public int GetY()
        {
            return View.Y;
        }

        #endregion
    }
}