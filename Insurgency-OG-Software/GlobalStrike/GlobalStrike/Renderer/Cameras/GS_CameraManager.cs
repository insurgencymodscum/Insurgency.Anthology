﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Viewports;
#endregion

namespace GlobalStrike.Renderer.Cameras
{
    public class GS_CameraManager : GameComponent
    {
        #region Data

        List<GS_Camera> Cameras;

        #endregion

        #region Initialization

        public GS_CameraManager(GS_Game game) : base(game)
        {
            Cameras = new List<GS_Camera>();
        }

        public override void Initialize()
        {

            base.Initialize();
        }

        #endregion

        #region Functions

        public void AddCamera(GS_Camera cam)
        {
            Cameras.Add(cam);
        }

        public void RemoveCamera(GS_Camera cam)
        {
            Cameras.Remove(cam);
        }

        public GS_Camera GetCamera(int i)
        {
            return Cameras.ElementAt<GS_Camera>(i);
        }

        public GS_Camera GetCurrentCamera()
        {
            return Cameras.ElementAt<GS_Camera>(Cameras.Count);
        }

        #endregion

        #region Update

        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }

        #endregion
    }
}