﻿
#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Logger;
using GlobalStrike.HUD;
#endregion

namespace GlobalStrike.Renderer.Cameras
{
    public class GS_FP_Camera : GS_Camera
    {
        #region Data

        GS_HUD_Item crosshair;
        GS_HUD_Item Crosshair
        {
            get { return crosshair; }
            set { crosshair = value; }
        }

        protected GS_Model fp_model;
        public GS_Model FP_Model
        {
            get { return fp_model; }
            set { fp_model = value; }
        }

        protected bool has_model;
        public bool ModelEnabled
        {
            get
            {
                if (fp_model != null) has_model = true;
                else has_model = false;
                return has_model;
            }
            set { has_model = value; }
        }

        #endregion

        #region Initialization

        public GS_FP_Camera(Vector3 pos, Vector3 target, GS_Viewport view)
            : base(pos, target, view)
        {

        }

        public GS_FP_Camera(Vector3 pos, Vector3 target, GS_Viewport view, GS_FP_Model model)
            : base(pos, target, view)
        {
            FP_Model = model;
        }

        #endregion       

        #region Functions

        public void MouseAction(MouseState prev)
        {
            GS_Log.addLine("MOUSE ACTION!", "GS_FPCamera");
            float yaw_angle =   (-MathHelper.PiOver4 / 150) *
                            (Mouse.GetState().X - prev.X);
            float pitch_angle = (MathHelper.PiOver4 / 100) *
                            (Mouse.GetState().Y - prev.Y);

            Direction = YawRotation(Direction, yaw_angle);
            Direction = PitchRotation(Direction, pitch_angle);
        }

        protected Vector3 YawRotation(Vector3 dir, float angle)
        {
            return Vector3.Transform(
                dir, Matrix.CreateFromAxisAngle(Up, angle));
        }

        protected Vector3 PitchRotation(Vector3 dir, float angle)
        {
            Vector3 Side = Vector3.Cross(Up, dir);

            return Vector3.Transform(
                dir, Matrix.CreateFromAxisAngle(Side, angle));
        }

        public void KeyboardAction()
        {
            GS_Log.addLine("KEYBOARD ACTION!", "GS_FPCamera");
        }

        public void CheckPositionChange()
        {
            // Move forward/backward
            if (Keyboard.GetState().IsKeyDown(Keys.W))
                Position += Direction * Speed;
            if (Keyboard.GetState().IsKeyDown(Keys.S))
                Position -= Direction * Speed;

            // Move side to side
            if (Keyboard.GetState().IsKeyDown(Keys.A))
                Position += Vector3.Cross(Up, Direction) * Speed;
            if (Keyboard.GetState().IsKeyDown(Keys.D))
                Position -= Vector3.Cross(Up, Direction) * Speed;
        }

        public void CheckRotationChange()
        {
            // Yaw rotation
            Direction = Vector3.Transform(Direction,
            Matrix.CreateFromAxisAngle(Up, (-MathHelper.PiOver4 / 150) *
            (Mouse.GetState().X - prevMouseState.X)));

            // Roll rotation
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                Up = Vector3.Transform(Up,
                Matrix.CreateFromAxisAngle(Direction,
                MathHelper.PiOver4 / 45));
            }
            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                Up = Vector3.Transform(Up,
                Matrix.CreateFromAxisAngle(Direction,
                -MathHelper.PiOver4 / 45));
            }

            // Pitch rotation
            Direction = Vector3.Transform(Direction,
                Matrix.CreateFromAxisAngle(Vector3.Cross(Up, Direction),
                (MathHelper.PiOver4 / 100) *
                (Mouse.GetState().Y - prevMouseState.Y)));

            Up = Vector3.Transform(Up,
                Matrix.CreateFromAxisAngle(Vector3.Cross(Up, Direction),
                (MathHelper.PiOver4 / 100) *
                (Mouse.GetState().Y - prevMouseState.Y)));

            if (prevMouseState.X != Mouse.GetState().X || prevMouseState.Y != Mouse.GetState().Y)
            {
                int new_x = Mouse.GetState().X;
                int new_y = Mouse.GetState().Y;
                if (Mouse.GetState().X < View.GetX())
                    new_x = View.GetX();
                if (Mouse.GetState().X > View.GetX() + View.GetWidth())
                    new_x = View.GetX() + View.GetWidth();
                if (Mouse.GetState().Y < View.GetY())
                    new_y = View.GetY();
                if (Mouse.GetState().Y > View.GetY() + View.GetHeight())
                    new_y = View.GetY() + View.GetHeight();

                Mouse.SetPosition(new_x, new_y);
            }

            // Reset prevMouseState
            prevMouseState = Mouse.GetState();
        }

        #endregion
    }
}
