﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.Logger;
#endregion

namespace GlobalStrike.Renderer.Cameras
{
    public class GS_Camera
    {
        #region Data

        protected bool mustTranslate = false;
        protected bool mustRotate = false;

        public Vector3 translate;
        public Vector3 Translate
        {
            get { return translate; }
            set
            {
                translate = value;
                mustTranslate = true;
            }
        }

        protected float pitch;
        public float Pitch
        {
            get { return pitch; }
            set
            {
                pitch = value;
                mustRotate = true;
            }

        }

        protected float yaw;
        public float Yaw
        {
            get { return yaw; }
            set
            {
                yaw = value;
                mustRotate = true;
            }

        }

        protected Vector2 lookVelocity;
        public Vector2 LookVelocity
        {
            get { return lookVelocity; }
            set { lookVelocity = value; }
        }

        protected float fov;
        public float FOV
        {
            get { return fov; }
            set { fov = MathHelper.ToRadians(value); }
        }

        protected float aspectRatio;
        public float AspectRatio
        {
            get { return aspectRatio; }
            set { aspectRatio = value; }
        }

        protected float nearDistance;
        public float NearDistance
        {
            get { return nearDistance; }
            set { nearDistance = value; }
        }

        protected float farDistance;
        public float FarDistance
        {
            get { return farDistance; }
            set { farDistance = value; }
        }

        // Camera vectors
        protected Vector3 position;
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        protected Vector3 target;
        public Vector3 Target
        {
            get { return target; }
            set { target = value; }
        }

        protected Vector3 direction;
        public Vector3 Direction
        {
            get { return direction; }
            set 
            {
                value.Normalize();
                direction = value; 
            }
        }

        public Vector3 Forward { get { return direction; } }
        public Vector3 Back { get { return Forward * -1; } }
        public Vector3 Right { get { return Vector3.Cross(Forward, Up); } }
        public Vector3 Left { get { return Right * -1; } }

        protected Vector3 up;
        public Vector3 Up 
        { 
            get { return up; }
            set { up = value; }
        }

        public Vector3 Down { get { return Up * -1; } }

        protected Matrix viewMatrix;
        public Matrix ViewMatrix
        {
            get
            {
                if (mustRotate || mustTranslate)
                {
                    if (mustRotate)
                    {
                        Matrix cameraViewRotationMatrix = Matrix.CreateRotationX(Pitch) * Matrix.CreateRotationY(Yaw);
                        Direction = Vector3.Transform(Direction, cameraViewRotationMatrix);
                        Pitch = 0.0f;
                        Yaw = 0.0f;
                        mustRotate = false;
                        DisplayCameraState();

                    }
                    if (mustTranslate)
                    {
                        Matrix cameraMoveRotationMatrix = Matrix.CreateRotationY(Yaw);
                        Position += Vector3.Transform(Translate, cameraMoveRotationMatrix);
                        Translate = new Vector3(0, 0, 0);
                        mustTranslate = false;
                        DisplayCameraState();
                    }
                    Target = Direction + Position;
                }
                viewMatrix = Matrix.CreateLookAt(Position, Target, Up);
                return viewMatrix;
            }
            set 
            {
                Translate = new Vector3(0, 0, 0);
                Yaw = 0.0f;
                Pitch = 0.0f;
                mustTranslate = false;
                mustRotate = false;
                viewMatrix = value; 
            }
        }

        protected Matrix projectionMatrix;
        public Matrix ProjectionMatrix
        {
            get
            {
                projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                            FOV, AspectRatio, NearDistance, FarDistance);
                return projectionMatrix;
            }
            set { projectionMatrix = value; }
        }
        
        protected GS_Viewport view;
        public GS_Viewport View
        {
            get { return view; }
            set 
            {
                HasViewport = true;
                view = value; 
            }
        }

        protected bool has_view = false;

        public bool HasViewport
        {
            get { return has_view; }
            set { has_view = value; }
        }

        protected float speed;
        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        //Rotation
        protected MouseState prevMouseState;

        #endregion

        #region Initialization

        public GS_Camera()
        {
            FOV = 45.0f;
            AspectRatio = 400 / 300;
            NearDistance = 1;
            FarDistance = 1000;
            Position = new Vector3(10, 10, 10);
            Target = new Vector3(0, 0, 0);
            Direction = Position + Target;
            Up = Vector3.Up;
            Speed = 2;
        }

        public GS_Camera(Vector3 pos, Vector3 targ)
        {
            FOV = 45.0f;
            AspectRatio = 400 / 300;
            NearDistance = 1;
            FarDistance = 1000;

            // View Matrix
            Position = pos;
            Target = targ;
            Direction = Position + Target;
            Up = Vector3.Up;
            Speed = 2;
        }

        // Create a camera that's assigned to a viewport
        public GS_Camera(Vector3 pos, Vector3 targ, GS_Viewport view)
        {
            FOV = 45.0f;
            NearDistance = 1;
            FarDistance = 1000;

            // View Matrix
            Position = pos;
            Target = targ;
            Direction = Position + Target;
            Up = Vector3.Up;
            Speed = 2;

            // Set Viewport
            View = view;

            // Set aspect ratio to that of the Viewport
            AspectRatio = (float)View.GetWidth() / (float)View.GetHeight();

            DisplayCameraState();
        }

        #endregion       

        #region Functions

        protected void DisplayCameraState()
        {
            GS_Log.addNewLine("Camera Info: ", "ViewportManager");
            GS_Log.addLine("\tPosition=" + Position.ToString(), "ViewportManager");
            GS_Log.addLine("\tTarget=" + Target.ToString(), "ViewportManager");
            GS_Log.addLine("\tDirection=" + Direction.ToString(), "ViewportManager");
            GS_Log.addLine("\tUp=" + Up.ToString(), "ViewportManager");
            GS_Log.addLine("\tSpeed=" + Speed.ToString(), "ViewportManager");
            GS_Log.addLine("\tFOV=" + FOV, "ViewportManager");
            GS_Log.addLine("\tAspectRatio=" + AspectRatio, "ViewportManager");
            GS_Log.addLine("\tNearPlane=" + NearDistance, "ViewportManager");
            GS_Log.addLine("\tFarPlane=" + FarDistance, "ViewportManager");
        }

        public void Move(Keys[] keys)
        {
            if (keys.Contains<Keys>(Keys.W))
            {
                Translate += (Forward * Speed);
                GS_Log.addLine("Forward Translation: (" + Translate.X + ", " + Translate.Y + ", " + Translate.Z + ")", "GS_Camera");
            }
            if (keys.Contains<Keys>(Keys.S))
            {
                Translate += (Back * Speed);
                GS_Log.addLine("Back Translation: (" + Translate.X + ", " + Translate.Y + ", " + Translate.Z + ")", "GS_Camera");
            }
            if (keys.Contains<Keys>(Keys.D))
            {
                Translate += (Right * Speed);
                GS_Log.addLine("Right Translation: (" + Translate.X + ", " + Translate.Y + ", " + Translate.Z + ")", "GS_Camera");
            }
            if (keys.Contains<Keys>(Keys.A))
            {
                Translate += (Left * Speed);
                GS_Log.addLine("Left Translation: (" + Translate.X + ", " + Translate.Y + ", " + Translate.Z + ")", "GS_Camera");
            }
        }

        public void Look(Vector2 velocity)
        {
            LookVelocity = new Vector2(velocity.X, velocity.Y);
            float sensitivity = 0.1f;

            Yaw -= LookVelocity.X * 0.05f * sensitivity;
            Pitch -= LookVelocity.Y * 0.05f * sensitivity;

            //Pitch = MathHelper.Clamp(Pitch, MathHelper.ToRadians(-89.9f), MathHelper.ToRadians(89.9f));


        }


        #endregion
    }
}