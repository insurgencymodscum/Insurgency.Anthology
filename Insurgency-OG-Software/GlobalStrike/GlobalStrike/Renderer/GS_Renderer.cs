using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Viewports;
//using GlobalStrike.Renderer.Input;
using GlobalStrike.Console;
using GlobalStrike.Logger;
using GlobalStrike.HUD;

namespace GlobalStrike.Renderer
{
    public class GS_Renderer : DrawableGameComponent
    {
        #region Components

        public GS_SceneManager SceneManager;
        public GS_ViewportManager ViewportManager;
        public SpriteBatch SpriteManager;
        //public GS_InputManager InputManager;
        //public GS_CameraManager CameraManager;

        #endregion

        #region Initialization

        public GS_Renderer(GS_Game game) : base(game)
        {
            SceneManager = new GS_SceneManager(game, this);
            ViewportManager = new GS_ViewportManager(game, this, 1);
            //InputManager = new GS_InputManager(game);
            //CameraManager = new GS_CameraManager(game);
        }

        public override void Initialize()
        {
            Game.Components.Add(SceneManager);
            GS_Log.addLine("Scene Manager Component Added", "GS_Renderer.cs");

            Game.Components.Add(ViewportManager);
            GS_Log.addLine("Viewport Manager Component Added", "GS_Renderer.cs");

            SpriteManager = new SpriteBatch(Game.GraphicsDevice);

            //Game.Components.Add(InputManager);
            //GS_Log.addLine("Input Component Added", "GS_Renderer.cs");

            base.Initialize();
        }

        #endregion

        #region Update and Draw

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            SpriteManager.Begin(
                SpriteBlendMode.AlphaBlend,
                SpriteSortMode.FrontToBack,
                SaveStateMode.None);
            foreach (GS_FP_Camera cam in ViewportManager.ViewportCameras)
            {
                GraphicsDevice.Viewport = cam.View.View;
                GraphicsDevice.Clear(Color.CornflowerBlue);

                foreach (GS_Model model in SceneManager.Models)
                    model.Draw(cam);

                foreach (GS_HUD_Item item in ViewportManager.HUDManager.Items)
                    SpriteManager.Draw(item.Texture, item.Position, item.Diffuse_Color);                

                base.Draw(gameTime);
            }

            SpriteManager.End();
        }

        #endregion
    }
}