﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Viewports;
#endregion

namespace GlobalStrike.Input
{
    public class GS_InputManager : GameComponent
    {
        #region Data

        /*
        Viewport Window;
        List<GS_Viewport> Viewports;
        public List<GS_FPCamera> ViewportCameras;
        int TotalPlayers;
        int border;
        */
        #endregion

        #region Initialization

        public GS_InputManager(GS_Game game) : base(game)
        {
            /*
            Viewports = new List<GS_Viewport>();
            ViewportCameras = new List<GS_FPCamera>();
            TotalPlayers = players;
            border = 1;
            GS_Log.addNewLine("New Viewport Manager Created", "GS_ViewportManager.cs");
            */
        }

        public override void Initialize()
        {

            base.Initialize();
        }

        #endregion

        #region Functions
        #endregion

        #region Update

        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }

        #endregion
    }
}