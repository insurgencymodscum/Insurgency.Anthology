﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Viewports;
#endregion

namespace GlobalStrike.Input
{
    public class GS_Xbox_InputManager : GS_InputManager
    {
        #region Data
        protected GamePadState gamepad;
        protected PlayerIndex player;
        #endregion

        #region Initialization

        public GS_Xbox_InputManager(GS_Game game) : base(game)
        {
            player = new PlayerIndex();
            gamepad = GamePad.GetState(player);
        }

        public override void Update(GameTime gameTime)
        {
            gamepad = GamePad.GetState(player);
            base.Update(gameTime);
        }

        #endregion 

        #region Functions
        #endregion
    }
}