﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using GlobalStrike;
using GlobalStrike.Console;
using GlobalStrike.Renderer;
using GlobalStrike.Renderer.Cameras;
using GlobalStrike.Renderer.Scene;
using GlobalStrike.Renderer.Viewports;
using GlobalStrike.Logger;
#endregion

namespace GlobalStrike.Input
{
    public class GS_PC_InputManager : GS_InputManager
    {
        #region Data
        public MouseState prevMouseState;
        public Rectangle bounds;
        public Vector2 Velocity;
        #endregion

        #region Initialization

        public GS_PC_InputManager(GS_Game game) : base(game)
        {
            bounds = new Rectangle(
                game.DeviceManager.GraphicsDevice.Viewport.X,
                game.DeviceManager.GraphicsDevice.Viewport.Y,
                game.DeviceManager.GraphicsDevice.Viewport.Width,
                game.DeviceManager.GraphicsDevice.Viewport.Height);

            Mouse.SetPosition(  bounds.X + bounds.Width / 2,
                                bounds.Y + bounds.Height / 2);

            prevMouseState = Mouse.GetState();
        }

        public override void Update(GameTime gameTime)
        {
            CheckInputs();
            base.Update(gameTime);
            prevMouseState = Mouse.GetState();
        }

        public void DetectKeyboardAction(GS_Camera cam)
        {
            if (Keyboard.GetState().GetPressedKeys().Length > 0)
            {
                cam.Move(Keyboard.GetState().GetPressedKeys());
            }

        }

        public void DetectMouseMovement(GS_Camera cam)
        {
            if (Mouse.GetState() != prevMouseState)
            {
                Velocity = new Vector2(
                    Mouse.GetState().X - prevMouseState.X,
                    Mouse.GetState().Y - prevMouseState.Y);

                cam.Look(Velocity);

                Mouse.SetPosition(bounds.X + bounds.Width / 2,
                                bounds.Y + bounds.Height / 2);
            }
        }

        protected String MovementType(Vector2 vector)
        {
            if ( vector.X > 0 )
            {
                if ( vector.Y > 0 )
                    return "Up-Right";
                else if ( vector.Y < 0 )
                    return "Down-Right";
                else
                    return "Right";
            }
            else if ( vector.X < 0 )
            {
                if ( vector.Y > 0 )
                    return "Up-Left";
                else if ( vector.Y < 0 )
                    return "Down-Left";
                else
                    return "Left";
            }
            else
            {
                if ( vector.Y >= 0 )
                    return "Up";
                else
                    return "Down";
            }
        }

        protected String MovementSpeed(Vector2 vector)
        {
            float magnitude;
            if (vector.X > 0 && vector.Y > 0)
                magnitude = vector.X * vector.Y;
            else if (vector.X == 0)
                magnitude = vector.Y;
            else
                magnitude = vector.X;

            if (magnitude < 0) magnitude *= -1;

            return "Speed Value: " + magnitude;
        }

        #endregion 

        #region Functions

        void CheckInputs()
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Game.Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Game.Exit();
        }

        #endregion
    }
}