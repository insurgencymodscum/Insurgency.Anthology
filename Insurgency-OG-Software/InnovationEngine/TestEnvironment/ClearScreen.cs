﻿using Innovation;
using Microsoft.Xna.Framework.Graphics;

namespace OG
{
    class ClearScreen : Component
    {
        Color color;

        public ClearScreen(Color color)
        {
            this.color = color;
        }

        // Override the component's draw method
        public override void Draw()
        {
            // Simply clear the backbuffer to red
            Engine.GraphicsDevice.Clear(color);
        }
    }
}