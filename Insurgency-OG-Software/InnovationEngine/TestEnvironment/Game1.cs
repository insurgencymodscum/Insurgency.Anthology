using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using Innovation;

namespace OG
{
    public class Game1 : Game
    {
        // The IGraphicsDeviceService the engine will use
        GraphicsDeviceManager graphics;

        public Game1()
        {
            // Setup graphics
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 200;
            graphics.PreferredBackBufferHeight = 150;

            Log.addLine("Graphics Device Manager Created...", "Game1.Game1");
        }

        protected override void UnloadContent()
        {
            Log.addLine("Creating Log File...", "Game1.UnloadContent");
            Log.compile(new FileStream(Log.path,
                FileMode.Create, FileAccess.Write));

            base.UnloadContent();
        }

        protected override void LoadContent()
        {
            // Setup engine. We do this in the load method
            // so that we know graphics will be ready for use
            Engine.SetupEngine(graphics);
            Log.addLine("Engine Loaded...", "Game1.LoadContent");

            // Create a new Camera
            FPSCamera camera = new FPSCamera();
            camera.Position = new Vector3(14.0f, 4.0f, 13.0f);
            camera.RotateTranslate(new Vector3(-.1417831f * 2f, .4225208f * 2, 0), Vector3.Zero);
            Engine.Services.AddService(typeof(Camera), camera);
            Log.addLine("Camera Loaded...", "Game1.LoadContent");

            // Setup physics
            Engine.Services.AddService(typeof(Physics), new Physics());
            Log.addLine("Physics Loaded...", "Game1.LoadContent");

            // NOTE: Creating Terrain is too expensive for work
            /*
            Terrain terrain = new Terrain(
                Engine.Content.Load<Texture2D>("Content/heightmap"),
                Engine.Content.Load<Texture2D>("Content/grass"));
            OG_Log.addLine("Terrain Created...", "Game1.LoadContent");
            */

            // Create the plane and make it immovable
            
            PhysicsActor plane = new PhysicsActor(
                Engine.Content.Load<Model>("Content/Maps/ig_plane"),
                new BoxObject(new Vector3(4, .01f, 4)));
            plane.PhysicsObject.Immovable = true;
            
            // Load the model we will use for our boxes
            Model model = Engine.Content.Load<Model>("Content/Cubes/ig_box");

            // Create the stack of boxes
            for (int y = 0; y < 3; y++)
                for (int x = 0; x < 3; x++)
                {
                    PhysicsActor act = new PhysicsActor(model, new BoxObject(
                        new Vector3(0.5f),
                        new Vector3(-.5f + (x * 0.52f), .5f + (y * 0.52f), -1),
                        Vector3.Zero));

                    act.Scale = new Vector3(.5f);
                }
            Log.addLine("Scene Loaded...", "Game1.LoadContent");
            
            MouseDevice mouse = new MouseDevice();
            Engine.Services.AddService(typeof(MouseDevice), mouse);
            Log.addLine("Mouse Loaded...", "Game1.LoadContent");

            KeyboardDevice keyboard = new KeyboardDevice();
            Engine.Services.AddService(typeof(KeyboardDevice), keyboard);
            Log.addLine("Keyboard Loaded...", "Game1.LoadContent");
        }

        protected override void Update(GameTime gameTime)
        {
            // Update the engine and game
            Engine.Update(gameTime);

            KeyboardDevice keyboard = Engine.Services.GetService<KeyboardDevice>();
            MouseDevice mouse = Engine.Services.GetService<MouseDevice>();
            FPSCamera cam = (FPSCamera)Engine.Services.GetService<Camera>();

            if (keyboard.IsKeyDown(Keys.Escape))
            {
                Log.addLine("Game is now exiting...", "Game1.Update");
                this.Exit();
            }

            Vector3 inputModifier = new Vector3(
                (keyboard.IsKeyDown(Keys.A) ? -1 : 0) + (keyboard.IsKeyDown(Keys.D) ? 1 : 0),
                (keyboard.IsKeyDown(Keys.Q) ? -1 : 0) + (keyboard.IsKeyDown(Keys.E) ? 1 : 0),
                (keyboard.IsKeyDown(Keys.W) ? -1 : 0) + (keyboard.IsKeyDown(Keys.S) ? 1 : 0)
            );

            cam.RotateTranslate(new Vector3(mouse.Delta.Y * -.002f, mouse.Delta.X * -.002f, 0), inputModifier * .5f);

            if (mouse.WasButtonPressed(MouseButtons.Left))
            {
                Log.addLine("Fire away!...", "Game1.Update");
                PhysicsActor act = new PhysicsActor(
                    Engine.Content.Load<Model>("Content/Cubes/ig_box"),
                    new BoxObject(new Vector3(2.5f), cam.Position, Vector3.Zero));

                act.Scale = new Vector3(0.5f);
                act.PhysicsObject.Mass = 1000;

                Vector3 dir = cam.Target - cam.Position;
                dir.Normalize();

                act.PhysicsObject.Velocity = dir * 100;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            // Draw the engine and game
            Engine.Draw(gameTime, ComponentType.All);
        }
    }
}