﻿using Microsoft.Xna.Framework.Graphics;

namespace Innovation
{
    public static class GraphicsUtil
    {
        // Creates a RenderTarget2D with the specified parameters
        public static RenderTarget2D CreateRenderTarget()
        {
            return CreateRenderTarget(Engine.GraphicsDevice.Viewport.Width,
                Engine.GraphicsDevice.Viewport.Height);
        }

        // Creates a RenderTarget2D with the specified parameters
        public static RenderTarget2D CreateRenderTarget(int Width, int Height)
        {
            return CreateRenderTarget(Width, Height,
                Engine.GraphicsDevice.DisplayMode.Format);
        }

        // Creates a RenderTarget2D with the specified parameters
        public static RenderTarget2D CreateRenderTarget(int Width, int Height,
            SurfaceFormat Format)
        {
            return CreateRenderTarget(Width, Height, Format,
                Engine.GraphicsDevice.PresentationParameters.MultiSampleQuality,
                Engine.GraphicsDevice.PresentationParameters.MultiSampleType);
        }

        // Creates a RenderTarget2D with the specified parameters
        public static RenderTarget2D CreateRenderTarget(int Width, int Height,
            SurfaceFormat Format, int MultiSampleQuality,
            MultiSampleType SampleType)
        {
            return new RenderTarget2D(Engine.GraphicsDevice, Width,
                Height, 1, Format, SampleType, MultiSampleQuality,
                RenderTargetUsage.DiscardContents);
        }

        // Creates a ResolveTexture2D
        public static ResolveTexture2D CreateResolveTexture()
        {
            return new ResolveTexture2D(Engine.GraphicsDevice,
                Engine.GraphicsDevice.Viewport.Width,
                Engine.GraphicsDevice.Viewport.Height, 1,
                Engine.GraphicsDevice.DisplayMode.Format);
        }
    }
}