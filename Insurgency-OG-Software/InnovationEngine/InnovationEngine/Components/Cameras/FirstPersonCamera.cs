﻿using Microsoft.Xna.Framework;
using System;

namespace Innovation
{
    // A simple first person camera. Accepts rotation and translation
    public class FPSCamera : Camera
    {
        // Keeps track of the rotation and translation that have been
        // added via RotateTranslate()
        Vector3 rotation;
        Vector3 translation;

        public FPSCamera(GameScreen Parent) : base(Parent) { }
        public FPSCamera() : base() { }

        // This adds to rotation and translation to change the camera view
        public void RotateTranslate(Vector3 Rotation, Vector3 Translation)
        {
            translation += Translation;
            rotation += Rotation;
        }

        public override void Update()
        {
            // Update the rotation matrix using the rotation vector
            Rotation = MathUtil.Vector3ToMatrix(rotation);

            // Update the position in the direction of rotation
            translation = Vector3.Transform(translation, Rotation);
            Position += translation;

            // Reset translation
            translation = Vector3.Zero;

            // Calculate the new target
            Target = Vector3.Add(Position, Rotation.Forward);

            // Have the base Camera update all the matrices, etc.
            base.Update();
        }
    }
}