﻿using JigLibX.Collision;
using JigLibX.Physics;

namespace Innovation
{
    public class Physics : Component
    {
        // The physics simulation
        public PhysicsSystem PhysicsSystem = new PhysicsSystem();

        // Whether or not we should update
        public bool UpdatePhysics = true;

        public Physics()
        {
            // Set up physics system
            this.PhysicsSystem.EnableFreezing = true;
            this.PhysicsSystem.SolverType = PhysicsSystem.Solver.Normal;
            this.PhysicsSystem.CollisionSystem = new CollisionSystemSAP();
        }

        public override void Update()
        {
            // Update the physics system
            if (UpdatePhysics)
                PhysicsSystem.CurrentPhysicsSystem.Integrate(
                    (float)Engine.GameTime.ElapsedGameTime.TotalSeconds);
        }
    }  
}